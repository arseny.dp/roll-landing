import Header from "../components/layout/header";
import Head from "next/head";
import Preface from "../components/sections/preface";
import { useEffect } from "react";
import { LanguageContextProvider } from "../components/context/languageContext";
import FAQ from "../components/sections/faq";
import Partners from "../components/sections/partners";
import DownloadApp from "../components/sections/downloadapp";
import Instruction from "../components/sections/instruction";
import Reviews from "../components/sections/reviews";
import Footer from "../components/layout/footer";

export default function Home({ lang="ru" }) {

  useEffect(() => {
    document.documentElement.lang = lang
  }, [lang])

  return (
    <LanguageContextProvider lang={lang}>
      <>
        <Head>
          <meta charSet="utf-8"/>
        </Head>
        <Header/>
        <Preface/>
        <FAQ/>
        <Partners id='partners'/>
        <DownloadApp/>
        <Instruction/>
        <Partners id='partners2'/>
        <Reviews/>
        <Footer/>
      </>
    </LanguageContextProvider>
  )
}