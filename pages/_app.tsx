import PropTypes from "prop-types";
import "../styles/globals.css";
import "tailwindcss/tailwind.css";
import React, { useEffect } from "react"
import Layout from "../components/layout/layout";
import { SidebarContextProvider } from "../components/context/sidebarConext";
import Head from 'next/head'
import { useRouter } from 'next/router'
import smoothscroll from 'smoothscroll-polyfill';

const MyApp: React.FC<{ Component; pageProps }> = ({
  Component = "",
  pageProps = "",
}) => {
  const router = useRouter()

  useEffect(() => {
    smoothscroll.polyfill();
    const appHeight = () => {
      let vh = window.innerHeight * 0.01
      document.documentElement.style.setProperty("--vh", `${vh}px`)
    }
    document.querySelectorAll('a[href^="#"]').forEach(anchor => {
        anchor.addEventListener('click', function (e) {
          let hashval = anchor.getAttribute('href')
          let target = document.querySelector(hashval)
          target.scrollIntoView({
              behavior: 'smooth',
              block: 'start'
          })

          history.pushState(null, null, hashval)
          e.preventDefault()
        });
    });
    window.addEventListener("resize", appHeight)
    appHeight()
    return () => {
      window.removeEventListener("resize", appHeight)
    }
    
  }, [])

  return (
    <SidebarContextProvider>
      <Head>
        {/* <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png"/> */}
        {/* <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png"/> */}
        {/* <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png"/> */}
        {/* <link rel="manifest" href="/site.webmanifest"/> */}
        {/* <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5"/> */}
        {/* <meta name="msapplication-TileColor" content="#da532c"/> */}
        {/* <meta name="theme-color" content="#EA4744"></meta> */}
        <title>Roll</title>
        <meta property="title" content="Roll" key="title" />
        <meta name="description" content="Твой проводник в мире стримингов"/>
        {/* <meta name="keywords" content="Margin trading, multi-chain platform, gas efficiency, leveraged trading, farming"/> */}
        {/* <meta property="og:image" content="https://marnotaur.com/meta.png"></meta> */}
        {/* <meta property='og:image:width' content='968'/> */}
        {/* <meta property='og:image:height' content='504'/> */}
        {/* <script src={`/google.js`}></script> */}
      </Head>
      <Layout>
        <Component {...pageProps} />
      </Layout>
    </SidebarContextProvider>
  ) 
};

MyApp.propTypes = {
  Component: PropTypes.any,
  pageProps: PropTypes.any,
};
MyApp.defaultProps = {
  Component: "",
  pageProps: "",
};

export default MyApp;
