import en from "./locales/en"
import ru from "./locales/ru"

export type ILanguages = "en" | "ru"

type I18n = {
    [key in ILanguages]: any
}

export const availableLangs:ILanguages[] = ["en", "ru"]

const i18n:I18n = {
    en,
    ru,
}

export default i18n