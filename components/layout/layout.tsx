import Footer from "./footer"
import Header from "./header"

const Layout = ({
    children
}) => {
    return (
        <>
            <div id="scroll" className='w-full bg-black overflow-hidden'>
                <div className={`text-sm xl:text-base xl:px-16 sm:px-12 px-4 max-w-desc mx-auto text-gr text-gray-main tracking-main font-main`}>
                    {children}
                </div>
            </div>
        </>
    )
}

export default Layout