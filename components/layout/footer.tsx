import links from "../data/links"
import Logo from "../icons/logo"
import AppLinks from "./applinks"
import useTransalte from "../hooks/useTranslate"

const Footer = () => {
    const { t } = useTransalte("indexPage")

    const linksTr = []
    for (let i = 0; i < links.length; i++) {
        linksTr.push(t(`menu${i+1}`))
    }
    return (
        <div className='grid grid-rows-1 gap-7.5 md:flex md:flex-row justify-between items-start md:items-center bg-black py-10 md:py-7.5 xl:py-10'>
            <a onClick={() => {
                    window.scrollTo({
                        behavior: "smooth",
                        top: 0,
                        left: 0
                    })
                    history.pushState(null, null, "#")
                }}
                className={`flex-shrink-0 flex cursor-pointer w-21 md:w-25 lg:w-27.5 relative`}>
                <Logo />
            </a>
            <div className={`flex md:ml-10 relative text-white`}>
                {links.map((link, index) => 
                    <a
                        key={link.scrollTo}
                        onClick={() => {
                            window.scrollTo({
                                top: document.getElementById(link.scrollTo).offsetTop,
                                behavior: "smooth"
                            })
                            history.pushState(null, null, `#${scrollTo}`)
                        }}
                    
                        className={`text-sm md:text-base mr-9 cursor-pointer transition-all `}>
                            {linksTr[index]}
                    </a>
                )}
            </div>
            <AppLinks footer />
        </div>
    )
}

export default Footer