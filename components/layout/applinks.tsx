import AppStoreIcon from "../icons/appstore"
import GooglePlayIcon from "../icons/googleplay"

const AppLinks = ({footer=false}) => {
    return (
        <div className={`inline-grid grid-cols-2 gap-4 ${footer ? 'md:grid-cols-1 xl:grid-cols-2' : ''}`}>
            <a target="_blank" className="block w-32 cursor-pointer transform hover:scale-110 focus:scale-110 transition text-black active:text-gray-hover">
                <AppStoreIcon />
            </a>
            <a target="_blank" className="block w-32 cursor-pointer transform hover:scale-110 focus:scale-110 transition text-black active:text-gray-hover">
                <GooglePlayIcon />
            </a>
        </div>
    )
}

export default AppLinks