import { useEffect, useState } from 'react'
import HeaderLink from './headerLink'
import links from '../data/links'
import useTransalte from "../hooks/useTranslate"
import Logo from '../icons/logo'
import MenuIcon from '../icons/menu'
import CloseIcon from '../icons/close'
import AppLinks from './applinks'
import LanguageSelector from './languageSelector'


const Header = () => {
    const [isSidebarOpen, setisSidebarOpen] = useState(false)
    const [scrolled, setScrolled] = useState(false)
    const { t } = useTransalte("indexPage")

    const linksTr = []
    for (let i = 0; i < links.length; i++) {
        linksTr.push(t(`menu${i+1}`))
    }
    

    useEffect(() => {
        const listner = (e) => {
            setScrolled(window.scrollY > 60)
        }
        window.addEventListener("scroll", listner)
        window.addEventListener("resize", listner)
    
        return () => {
            window.removeEventListener("scroll", listner)
            window.removeEventListener("resize", listner)
        }
    }, [])
    return (
        <>
            <div className={`fixed top-0 left-0 w-full z-50`}>
                <div className={`relative xl:px-16 sm:px-12 px-4 flex items-center justify-between transition-all duration-300 text-white ${scrolled ? 'py-4' : 'xl:py-10 sm:py-4 py-10'}`}>
                    <div className={`absolute h-full bg-black left-0 top-0 w-full transition-all ${scrolled ? " blur-lg backdrop-blur-md bg-opacity-30" : " blur-none backdrop-blur-none bg-opacity-0"} backdrop-filter`} />
                    <a onClick={() => {
                            window.scrollTo({
                                behavior: "smooth",
                                top: 0,
                                left: 0
                            })
                            history.pushState(null, null, "#")
                        }}
                        className={`flex-shrink-0 flex cursor-pointer w-21 md:w-25 lg:w-27.5 relative`}>
                        <Logo />
                    </a>
                    <div className={`hidden md:flex ml-10 relative`}>
                        {links.map((link, index) => <HeaderLink key={link.scrollTo} setisSidebarOpen={setisSidebarOpen} text={linksTr[index]} scrollTo={link.scrollTo} index={index} /> )}
                    </div>
                    <div
                        onClick={() => {
                            console.log(isSidebarOpen)
                            setisSidebarOpen(true)
                        }}
                        className="block md:hidden w-7 relative">
                        <MenuIcon />
                    </div>
                    <div className="invisible flex md:visible items-center absolute md:relative">
                        <div className="mr-4 flex invisible lg:visible absolute lg:static">
                            <AppLinks />
                        </div>
                        <LanguageSelector />
                    </div>
                </div>
            </div>
            
            <div className={`fixed z-40 bg-black top-0 left-0 w-full h-full transition-all duration-300 ${isSidebarOpen ? "visible opacity-25" : "invisible opacity-0"} `}/>
            
            <div className={`fixed w-full h-full top-0 left-0 z-50 pointer-events-none text-white`}>
                <div className={`relative w-full p-5 transition-all duration-500 bg-black pointer-events-auto`}
                    style={{
                        marginTop: isSidebarOpen ? "0" : "-100%"
                    }}>
                    <div
                        onClick={() => {
                            setisSidebarOpen(false)
                        }}
                        className={`absolute top-10 right-5 h-8 w-7`}>
                        <CloseIcon />
                    </div>
                    <a onClick={() => {
                        window.scrollTo({
                            behavior: "smooth",
                            top: 0,
                            left: 0
                        })
                        history.pushState(null, null, "#")
                        setisSidebarOpen(false)
                        }}
                        className={`flex-shrink-0 flex cursor-pointer w-21 mb-10`}>
                        <Logo />
                    </a>
                    <div className={`grid grid-cols-1 gap-5 mb-7`}>
                        {links.map((link, index) =>
                            <HeaderLink key={link.scrollTo} setisSidebarOpen={setisSidebarOpen} text={linksTr[index]} scrollTo={link.scrollTo} index={index} />
                        )}
                    </div>
                    <LanguageSelector />
                </div>
            </div>
        </>
    )
}

export default Header
