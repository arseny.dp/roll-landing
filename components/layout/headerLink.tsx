import { useContext } from "react"
import SidebarContext from "../context/sidebarConext"

const HeaderLink = (props) => {

    const {scrollTo=null, text="", index=0, setisSidebarOpen=null} = props
    const {activeElement} = useContext(SidebarContext)

    return (
        <a
            onClick={() => {
                window.scrollTo({
                    top: document.getElementById(scrollTo).offsetTop,
                    behavior: "smooth"
                })
                if (setisSidebarOpen) setisSidebarOpen(false)
                history.pushState(null, null, `#${scrollTo}`)
            }}
            
            className={`text-sm md:text-base mr-9 cursor-pointer transition-all ${activeElement !== index + 1 ? "" : "text-red-main"} `}>
            {text}
        </a>
    )
}

export default HeaderLink