const DecorGradient = (props) => {

    const {className, color = "red", opacityPerc = "30"} = props
    
    return (
        <div className={`absolute pointer-events-none ${className} bg-radial-decor-${color} filter blur-3xl opacity-${opacityPerc}`}>
            <div className='pt-full'></div>
        </div>
    )
}

export default DecorGradient