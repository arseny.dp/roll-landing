import CurvedArrowIcon from "../icons/curvedArrow"
import SubHeroText from "../textStyles/subHeroText"

const InstructionItem = ({index, caption, text='', last=false}) => {

    return (
        <div className='flex items-center even:flex-row-reverse mb-10'>
            <img src={`how-screen-${index}.png`} alt='' className={index%2 ? 'pr-4' : 'pl-4'}/>
            <div className={`flex flex-col items-start justify-center max-w-sm relative ${index%2 ? 'ml-8' : 'mr-8'}`}>
                {index ? <div className='flex text-white px-4 py-3 mb-4 rounded-md bg-red-main'>Шаг {index}</div> : ''}
                <SubHeroText>{caption}</SubHeroText>
                {text ? <div className='pt-5'>{text}</div> : ''}
                <div className={`${last ? 'hidden' : ''} absolute pt-20 top-full left-9 ${index%2 ? '' : 'transform scale-x-mirror'}`}><CurvedArrowIcon /></div>
            </div>
        </div>
    )
}
export default InstructionItem