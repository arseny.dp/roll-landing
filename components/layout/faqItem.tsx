import { useState } from "react"
import ChevronIcon from "../icons/chevron"

const FAQItem = ({caption='', children}) => {
    const [isOpened, setIsOpened] = useState(false)

    return (
        <>
            <details className="border-b border-opacity-20 border-white text-white py-7.5">
                <summary className=" flex justify-between items-center cursor-pointer text-lg md:text-xl xl:text-2xl list-none group"
                onClick={() => {
                    setIsOpened(!isOpened)
                }}>
                    {caption}
                    <div className="bg-gray-border rounded-round transition-all transform w-7.5 xl:w-9 h-7.5 xl:h-9 flex items-center justify-center group-hover:scale-110">
                        <div className={`w-4/6 h-4/6 transition-transform ${isOpened ? '' : ' transform rotate-180' }`}><ChevronIcon /></div>
                    </div>
                </summary>
                <div className="pt-5 xl:pt-7.5 xl:mb-2.5 max-w-none md:max-w-lg xl:max-w-3xl text-white text-opacity-70">
                    {children}
                </div>
            </details>
        </>
    )
}
export default FAQItem
