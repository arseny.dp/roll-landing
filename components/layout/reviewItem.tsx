import StarIcon from "../icons/star"

const ReviewItem = ({name, rating, caption, text}) => {
    const star = <div className='w-5 h-5 mr-0.5'><StarIcon /></div>

    const stars = []
    for (let index = 0; index < rating ; index++) { stars.push(star) }

    return (
        <div className='bg-gray-review rounded-review p-5 mr-4 last:mr-0 w-64 flex-grow flex-shrink-0'>
            <div className='flex mb-5'>
                <div className='w-14.5 bg-white bg-opacity-20 rounded-round flex items-center justify-center mr-3'><img src='portraitStub.png' alt='' /></div>
                <div>
                    <div className="text-white mb-1.5 text-xs md:text-sm xl:text-base">{name}</div>
                    <div className='flex'>{stars}</div>
                </div>
            </div>
            <div className='text-white mb-3'>{caption}</div>
            <div>{text}</div>
        </div>
    )
}

export default ReviewItem