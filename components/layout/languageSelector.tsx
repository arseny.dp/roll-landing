import { useContext, useState } from "react"
import { availableLangs } from "../../i18n"
import LanguageContext from "../context/languageContext"
import useTransalte from "../hooks/useTranslate"
import ChevronIcon from "../icons/chevron"

const LanguageSelector = () => {
    const {activeLang, setActiveLang} = useContext(LanguageContext)
    const [isLangSwitchOpen, setIsLangSwitchOpen] = useState(false)
    const { t } = useTransalte("indexPage")

    return (
        <div onClick={(e) => {
            e.stopPropagation()
            setIsLangSwitchOpen(!isLangSwitchOpen)
        }} className="relative text-sm md:text-base flex">
            <div className={`cursor-pointer px-3 py-2.5 uppercase flex items-center rounded-md border transition transform border-gray-border hover:bg-gray-lang hover:scale-110 ${isLangSwitchOpen ? ' bg-gray-lang text-gray-main' : ''}`}>
                <div className='mr-2.5'>{activeLang}</div>
                <div className={`w-6 h-6 transition-transform ${isLangSwitchOpen ? '' : ' transform rotate-180'}`}><ChevronIcon /></div>
            </div>
            <div className={`absolute top-full overflow-hidden mt-2.5 md:right-0 transition-all duration-300 bg-gray-lang rounded-md ${isLangSwitchOpen ? "visible opacity-100" : "invisible opacity-0"}`}>
            {availableLangs.map(lang => (
                <div className="py-2 mx-2.5 border-b border-gray-main border-opacity-10 last:border-b-0"
                key = {lang}>
                    <div onClick={() => {
                        // router.push(`/${lang}`)
                        setActiveLang(lang)
                    }} className='flex items-center cursor-pointer py-1 px-2.5 -mx-2.5 hover:bg-gray-select'>
                        <img className="mr-1.5 block max-w-none" src={`flag-${lang}.png`} alt="" />
                        <div>{t(lang)}</div>
                    </div>
                </div>
            ))}
            </div>
        </div>
    )
}

export default LanguageSelector