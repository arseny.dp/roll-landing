import AmazonIcon from "../icons/amazon"
import NetflixIcon from "../icons/netflix"
import SlackIcon from "../icons/slack"
import ZoomIcon from "../icons/zoom"

const partnersList = [
    { name:'Slack', link:'https://slack.com/', icon:<SlackIcon /> },
    { name:'Amazon', link:'https://www.amazon.com/', icon:<AmazonIcon /> },
    { name:'Zoom', link:'https://explore.zoom.us/', icon:<ZoomIcon /> },
    { name:'Netflix', link:'https://www.netflix.com/', icon: <NetflixIcon />},
    { name:'Amazon', link:'https://www.amazon.com/', icon:<AmazonIcon /> },
    { name:'Slack', link:'https://slack.com/', icon:<SlackIcon /> },
]

export default partnersList