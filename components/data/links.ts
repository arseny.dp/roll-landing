const links = [
    
    {
        link: "Faq",
        scrollTo: "faq"
    },
    {
        link: "Partners",
        scrollTo: "partners"
    },
    {
        link: "Reviews",
        scrollTo: "reviews"
    },
]   

export default links