const HeartIcon = () => 
    <svg width="25" height="25" className={`w-full h-full`} viewBox="0 0 25 25" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M23.8674 7.10942C21.3674 0.468797 13.2425 3.43755 12.5003 6.75786C11.4846 3.24223 3.55495 0.625046 1.13308 7.10942C-1.56224 14.336 11.5628 20.8204 12.5003 21.875C13.4378 21.0157 26.5628 14.2188 23.8674 7.10942Z"
            className={`fill-current`} />
    </svg>

export default HeartIcon