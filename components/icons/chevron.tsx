const ChevronIcon = () => 
    <svg width="24" height="24" className={`w-full h-full`} viewBox="0 0 25 24" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path d="M12.4998 8.28691L6.48977 14.2969L7.90277 15.7119L12.4998 11.1119L17.0958 15.7119L18.5098 14.2979L12.4998 8.28691Z"
        className={`fill-current`}/>
    </svg>

export default ChevronIcon