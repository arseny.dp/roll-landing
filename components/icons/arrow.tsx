const ArrowIcon = () => 
    <svg width="60" height="60" className={`w-full h-full`} viewBox="0 0 60 60" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path d="M39.7677 33.2797H7V26.7202H39.7677L31.2506 18.6383L36.1386 14L53 30L36.1386 46L31.2506 41.3617L39.7677 33.2797Z"
        className={`fill-current`}/>
    </svg>

export default ArrowIcon