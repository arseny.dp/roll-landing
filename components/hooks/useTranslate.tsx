import { useContext } from "react"
import i18n from "../../i18n"
import LanguageContext from "../context/languageContext"

const useTranslate = (section) => {
    const {activeLang} = useContext(LanguageContext)
    const current = i18n[activeLang]
    const t = (key) => {
        if (current && current[section] && current[section][key]) {
            return current[section][key]
        }
        else {
            return i18n.en[section][key]
        }
    }
    return {t}
}

export default useTranslate