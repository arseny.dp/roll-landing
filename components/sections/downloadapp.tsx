import AppLinks from "../layout/applinks"
import HeroText from "../textStyles/heroText"

const DownloadApp = () => 
    <div className='mb-25'>
        <div className="mb-6 md:mb-7.5 xl:mb-8">
            <HeroText>Скачай приложение рекомендательного сервиса и можно смотреть</HeroText>
        </div>
        <div className="mb-7.5 md:mb-9 xl:mb-10">Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. Exercitation veniam consequat sunt nostrud amet.</div>
        <AppLinks />
        <div className="hidden md:flex justify-between pt-8 xl:pt-14">
            <img src="app-screen-1.png" alt="" className="filter drop-shadow-phone w-1/4 xl:w-auto " />
            <img src="app-screen-2.png" alt="" className="filter drop-shadow-phone w-1/4 xl:w-auto  " />
            <img src="app-screen-3.png" alt="" className="filter drop-shadow-phone w-1/4 xl:w-auto " />
        </div>
    </div>

export default DownloadApp