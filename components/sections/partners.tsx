import HeroText from "../textStyles/heroText"
import partnersList from "../data/partnersList"

const Partners = ({id}) => 
    <div id={id ? id : 'partners'} className="pt-10 mb-28">
        <HeroText>Наши партнеры</HeroText>
        <div className="pt-10 grid gap-y-14 grid-cols-2 sm:grid-cols-4 justify-items-center lg:flex lg:justify-between">
            {partnersList.map((item, index) => {
                return(
                    <a href={item.link} target="_blank" className="hover:text-white transition-colors h-8"
                        key={item.name+index}>
                        {item.icon}
                    </a>
                )
            })}
        </div>
    </div>

export default Partners