import DecorGradient from "../layout/decorGradient"
import FAQItem from "../layout/faqItem"
import HeroText from "../textStyles/heroText"

const FAQ = () => {
    return (
        <div className="mb-14 pt-7.5 relative" id="faq">
            <DecorGradient className="w-9/12 max-w-3xl -top-52 -left-72" opacityPerc="50"/>
            <div className="mb-2.5 xl:mb-5 relative">
                <HeroText>Частые вопросы</HeroText>
            </div>
            <div className="relative">
                <FAQItem caption="Бесплатно ли приложение?">
                    Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. Exercitation veniam consequat sunt nostrud amet.
                </FAQItem>
                <FAQItem caption="Бесплатно ли приложение?">
                    Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. Exercitation veniam consequat sunt nostrud amet.
                </FAQItem>
                <FAQItem caption="Бесплатно ли приложение?">
                    Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. Exercitation veniam consequat sunt nostrud amet.
                </FAQItem>
                <FAQItem caption="Бесплатно ли приложение?">
                    Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. Exercitation veniam consequat sunt nostrud amet.
                </FAQItem>
            </div>
        </div>
    )
}

export default FAQ