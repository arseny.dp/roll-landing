import DecorGradient from "../layout/decorGradient"
import ArrowIcon from "../icons/arrow"
import CheckAllIcon from "../icons/checkall"
import HeartIcon from "../icons/heart"
import AppLinks from "../layout/applinks"
import HeroText from "../textStyles/heroText"

const Preface = () => {
    return (
        <div className="grid md:grid-cols-2 lg:min-h-screen relative items-center">
            <DecorGradient className="w-9/12 max-w-4xl -top-14 left-1/2"/>
            <div className="pt-64 lg:pt-0 pb-36 lg:pb-0 relative">
                <HeroText>
                    <div>Твой проводник </div>
                    <div className="flex items-center">
                        <div className="w-8 h-8 sm:w-9 sm:h-9 lg:w-15 lg:h-15 text-red-main"><ArrowIcon /></div> в мире стримингов
                    </div>
                </HeroText>
                <div className="pt-7 mb-8">
                    Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. Exercitation veniam consequat sunt nostrud amet.
                </div>
                <AppLinks />
            </div>
            <div className="pt-32 pb-36 lg:pt-0 lg:pb-0">
                <div className="relative">
                    <img className="-ml-24" src="preface-decor.png" alt="" />
                    <div className="absolute flex items-center px-3.5 py-3 rounded-xl top-64 left-2 text-white bg-purple-accent">
                        <div className="w-6 h-6 mr-3 text-yellow-accent"><HeartIcon /></div>
                        Настрой предпочтения
                    </div>
                    <div className="absolute flex items-center px-3.5 py-3 rounded-xl bottom-48 right-40 text-gray-accent bg-yellow-accent">
                        <div className="w-6 h-6 mr-3 text-purple-accent"><CheckAllIcon /></div>
                        Смотри любые трейлеры
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Preface