import InstructionItem from '../layout/instructionItem'
import HeroText from '../textStyles/heroText'

const Instruction = () => 
    <div className='pt-10'>
        <HeroText>Как работает Roll?</HeroText>
        <div className='max-w-5xl mx-auto pt-32'>
            <InstructionItem index='1' caption='Скачай приложение' />
            <InstructionItem index='2' caption='Настрой предпочтения' text='Ответь на пару вопросов нашего бота, чтобы он собрал для тебя уникальную подборку трейлеров' />
            <InstructionItem index='3' caption='Смотри любые трейлеры' />
            <InstructionItem index='4' caption='Выбери лучший стриминг' text='Смахни экран влево и узнай в каких стримингах выгоднее смотреть' />
            <InstructionItem index='5' caption='Собери свою коллекцию трейлеров' />
            <InstructionItem index='6' caption='Ставь рейтинг и комментируй трейлеры. Это поможет нашему боту' last/>
        </div>
    </div>

export default Instruction