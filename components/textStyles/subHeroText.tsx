const SubHeroText = ({ children }) => {
    return (
        <div className={`text-2xl sm:text-3xl lg:text-5xl whitespace-pre-wrap text-white tracking-normal`}>
            {children}
        </div>
    )
}

export default SubHeroText