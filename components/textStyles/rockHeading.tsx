const RockHeading = ({ children }) => {
    return (
        <h3 className={`text-xl text-gold-main font-greek`}>
            {children}
        </h3>
    )
}

export default RockHeading