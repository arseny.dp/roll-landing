const HeroText = ({ children }) => {
    return (
        <h2 className={`text-2xl sm:text-3xl lg:text-5xl whitespace-pre-wrap text-white tracking-normal`}>
            {children}
        </h2>
    )
}

export default HeroText