import React, { useState } from "react";
import { ILanguages } from "../../i18n";



const LanguageContext = React.createContext({
    activeLang: "ru",
    setActiveLang: (arg: ILanguages) => {}
});

export const LanguageContextProvider = ({lang, children}) => {
    const [activeLang, setActiveLang] = useState(lang)
    console.log(activeLang)
    return (
        <LanguageContext.Provider value={{
            activeLang,
            setActiveLang
        }}>
            {children}
        </LanguageContext.Provider>
    )
}
export default LanguageContext