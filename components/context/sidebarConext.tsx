import React, { ReactNode, useEffect, useState } from "react";
import useWindowSize from "../hooks/useWindowSize";
import links from "../data/links";

const SidebarContext = React.createContext({
    activeElement: 0,
});

interface Props {
  children: ReactNode;
}

const SidebarContextProvider = ({ children }: Props) => {
  const [activeElement, setActiveElement] = useState(0)
  const {height, width} = useWindowSize()


  useEffect(() => {
    const listner = (e) => {
      const positions = [0]
      const goal = window.scrollY
      links.map((link) => {
        if (document.getElementById(link.scrollTo)) {
          positions.push(document.getElementById(link.scrollTo).offsetTop ) 
        }
      })
      var closest = positions.reduce(function(prev, curr) {
        return (Math.abs(curr - goal) < Math.abs(prev - goal) ? curr : prev);
      });
      setActiveElement(positions.indexOf(closest));
    }
    window.addEventListener("scroll", listner)

    return () => {window.removeEventListener("scroll", listner)}
  }, [height, width])


  return (
    <SidebarContext.Provider
    value={{
        activeElement,
    }}
    >
      {children}
    </SidebarContext.Provider>
  );
};

export default SidebarContext;

export { SidebarContextProvider };