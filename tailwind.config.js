const typography = require("tailwindcss-typography");

module.exports = {
  purge: ["./components/**/*.{js,ts,jsx,tsx}", "./pages/**/*.{js,ts,jsx,tsx}", "./sections/**/*.{js,ts,jsx,tsx}"],
  darkMode: false, // or 'media' or 'class'
  theme: {
    textShadow: {},
    extend: {
      spacing: {
        7.5: "1.875rem",
        14.5: "3.625rem",
        15: "3.75rem",
        21: "5.25rem",
        25: "6.25rem",
        27.5: "6.875rem",
      },
      colors: {
        red: {
          main: "#EA4744",
        },
        gray: {
          hover: "#1A1A1A",
          accent: "#484848",
          border: "#464646",
          main: "#969696",
          lang: "#191919",
          select: "#232323",
          review: "#1E1E1E",
        },
        "purple-accent": "#4D57C7",
        "yellow-accent": "#F6DA6E",
      },
      backgroundImage: {
        "radial-decor-red": "radial-gradient(50% 50% at 50% 50%, #E94944 0%, rgba(233, 73, 68, 0.817708) 18.23%, rgba(233, 73, 68, 0) 100%)",
        "radial-decor-orange": "radial-gradient(50% 50% at 50% 50%, #FF8A00 0%, rgba(255, 138, 0, 0.817708) 18.23%, rgba(255, 138, 0, 0) 100%)",
      },
      padding: {
        "full": "100%",
      },
      maxWidth: {
        "desc": "1440px",
      },
      letterSpacing: {
        main: "-0.015em",
      },
      fontFamily: {
        main: ["Inter", "sans-serif"],
      },
      borderRadius: {
        round: "50%",
        review: "1.25rem",
      },
      dropShadow: {
        phone: ["-50.7726px 47.5993px 63.4658px rgba(0, 0, 0, 0.16)", "-18.7704px 17.7704px 38.0794px rgb(0 0 0 / 16%)"],
      },
      scale: {
        mirror: '-1',
      },
    },
  },
  variants: {
    extend: {
      scale: ['hover, group-hover, active'],
      textColor: ['active'],
      borderWidth: ['last'],
      flexDirection: ['even'],
      margin: ['last'],
    },
  },
  plugins: [
    typography({
      // all these options default to the values specified here
      ellipsis: true, // whether to generate ellipsis utilities
      hyphens: true, // whether to generate hyphenation utilities
      kerning: true, // whether to generate kerning utilities
      textUnset: true, // whether to generate utilities to unset text properties
      componentPrefix: "c-", // the prefix to use for text style classes
    }),
  ],
};
