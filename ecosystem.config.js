module.exports = {
  apps : [{
    name: 'roll',
    script: 'npm run dev',
    instances: 1,
    autorestart: true,
    watch: false,
    max_memory_restart: '1G',
  }],

  deploy : {
    production : {
      user : 'pro',
      host : '135.181.42.202',
      ref  : 'origin/master',
      repo : 'git@gitlab.com:cinema9/roll-landing.git',
      path : '~/roll-landing',
      'pre-deploy' : '../pre-deploy.sh',
      'post-deploy' : '../post-deploy.sh',
    }
  }
};
